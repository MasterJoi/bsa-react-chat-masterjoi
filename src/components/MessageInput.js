import React, { useState, useEffect } from 'react';
import { Navbar } from 'react-bootstrap';
import Container from "react-bootstrap/Container";
import { Form } from 'react-bootstrap';
import Button from "react-bootstrap/Button";

function createUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

const MessageInput = ({
    addMessageHandler,
    currentUser,
    messageToEdit = "",
    editMessageHandler,
    isEditMode
}) => {
    const [message, setMessage] = useState(isEditMode ? messageToEdit.text : null);

    useEffect(() => {
        messageToEdit ? setMessage(messageToEdit.text) : setMessage(null)
    }, [messageToEdit]);

    const fullMessageObject = {
        ...currentUser,
        "id": 0,
        "text": "",
        "createdAt": new Date().toISOString(),
        "editedAt": ""
    }

    const newMessage = () => {
        addMessageHandler({
            ...fullMessageObject,
            text: message,
            id: createUUID()
        });
        setMessage("");
    }

    const editMessage = () => {
        editMessageHandler({
            ...fullMessageObject,
            text: message,
        });
        setMessage("");
    }

    const handleChange = (event) => {
        setMessage(event.target.value);
    };

    return (
        <Navbar className='message-input' fixed="bottom">
          <Container>
            <Form
              inline
              className="w-100 d-flex justify-content-between align-items-center"
            >
                    <Form.Group className='message-input-text' style={{ flex: 1 }}>
                <Form.Control
                  value={message}
                  style={{ width: "100%", marginRight: "5px" }}
                  required
                  type="text"
                  placeholder="Type Message here..."
                  onChange={handleChange}
                />
              </Form.Group>
                    <Button className='message-input-button' variant="dark" type="button" onClick={isEditMode ? editMessage : newMessage}>
                        {isEditMode ? "Edit" : "Send"}
              </Button>
            </Form>
          </Container>
        </Navbar>
    )
}

export default MessageInput;