import * as React from "react";
import Container from "react-bootstrap/Container";
import Message from "./Message";
import OwnMessage from "./OwnMessage";
import moment from 'moment';
import { Card } from "react-bootstrap";


const MessageList = ({ messages, currentUser, likeMessageHandler, editModeHandler, deleteMessageHandler }) => {

    return (
        <Container>
            <div className="message-list">
                {
                    <ul className="list-group" style={{
                        marginBottom: "60px", width: "100%",
                        marginTop: "10px" }}>
                        {messages.length > 0 ? (
                            messages.map((item, i) => {
                                const isCurrentUser = currentUser.userId === item.userId;
                                let dateForUser = item.createdAt.slice(11, 16);

                                let previousDate = "";
                                if (i > 0) {
                                    previousDate = moment(messages[i - 1].createdAt.slice(0, 10)).format(
                                        "L"
                                    );
                                } else {
                                    previousDate = moment('2020-07-15').format("L");
                                }

                                let currentDate = moment(item.createdAt.slice(0, 10)).format("L");

                                        if (!isCurrentUser) {
                                            return (
                                                <div style={{ alignSelf: "flex-start" }}>
                                                    {previousDate && !moment(currentDate).isSame(previousDate, "day") ? (<Card className="w-100 d-flex justify-content-between align-items-center">{moment(currentDate).format('LL')}</Card>) : null}
                                                    <Message item={item} onLike={likeMessageHandler} date={dateForUser} index={i} />
                                                </div>
                                            )
                                            
                                        } else {
                                            return (
                                                <div style={{ alignSelf: "flex-end" }}>
                                                    {previousDate && !moment(currentDate).isSame(previousDate, "day") ? (<Card className="w-100 d-flex justify-content-between align-items-center">{moment(currentDate).format('LL')}</Card>) : null}
                                                    <OwnMessage item={item} onEdit={editModeHandler} onDelete={deleteMessageHandler} date={dateForUser} index={i} />
                                                </div>
                                            )
                                        }
                            })) : (
                            <div className="text-center mt-5 pt-5">
                                <p className="lead text-center">Fetching Messages</p>
                            </div>
                        )}
                    </ul>
                }
            </div>
        </Container>
    )
}

export default MessageList;