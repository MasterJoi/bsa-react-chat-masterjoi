import * as React from "react";
import { Navbar } from 'react-bootstrap';
import Container from "react-bootstrap/Container";
import 'bootstrap/dist/css/bootstrap.min.css';

const Header = ({ messages }) => {
    let messagesNumber = messages.length;
    
    let userNames = messages.map((elem) => {return elem.user;});
    let uniqueUserNames = userNames.filter((value, index, self) => {
            return self.indexOf(value) === index;});
    let participantsNumber = uniqueUserNames.length;  
    let lastMessageDate = messages[messagesNumber - 1].createdAt;
    let date = lastMessageDate.slice(0, 10) + " " + lastMessageDate.slice(11, 16);
    
    return (
        <Container className='header'>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="#home">MasterJOI`s Chat</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Signed in as: <a href="#login">Kyryll Hlum</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
            <div className="d-flex align-items-center justify-content-between">
                <h3 className="header-title text-center py-3 d-inline">
                    Task Chat
                </h3>
                <span className='header-users-count'>{participantsNumber} participants</span>
                <span className='header-messages-count'>{messagesNumber} messages</span>
                <span className='header-last-message-date'>last message at: {date}</span>
            </div>
        </Container>
    )
}
export default Header;