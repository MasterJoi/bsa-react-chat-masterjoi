import * as React from "react";
import { Spinner } from "react-bootstrap";

const Preloader = () => {
    return <div className="preloader" >
        <Spinner animation="grow" />
    </div>
}


export default Preloader;