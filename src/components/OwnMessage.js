import * as React from "react";
import Button from "react-bootstrap/Button";
import Avatar from 'react-avatar';
import { TrashFill, Pencil} from 'react-bootstrap-icons';


const CardFooter = {
    width: "100%",
    display: "flex",
    alignItems: "center",
}

const CardFotterChildStyle = {
    flex: "1",
}

const OwnMessage = ({ item, onEdit, onDelete, date, index}) => {

    return (
        <li className="own-message list-group-item" key={item.userId} style={{ alignSelf: "flex-end", margin: "10px", border: "1px solid #1d1f1c", borderRadius: "30px" }}>
            <Avatar className='message-user-avatar' style={{ marginRight: "10px" }} size="50" src={item.avatar} round={true} />
            <strong className='message-user-name'>{item.user}</strong>
            <p className='message-text'>{item.text}</p>
            <div style={CardFooter}>
                <div style={CardFotterChildStyle} >
                    {
                        <Button className='message-delete' variant="dark" onClick={() => { onDelete(item.id) }} style={{ margin: "5px" }}>
                            <TrashFill />
                        </Button>
                    }
                    {
                        <Button className='message-edit' variant="dark" onClick={() => { onEdit(index) }} style={{ margin: "5px" }}>
                            <Pencil />
                        </Button>
                    }
                </div>
                <footer className="blockquote-footer">
                    <cite>{date}</cite>
                </footer>
            </div>
        </li >
    )
}

export default OwnMessage;
