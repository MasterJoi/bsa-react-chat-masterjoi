import React, { Component } from 'react';
import Header from './components/Header';
import MessageList from './components/MessageList';
import MessageInput from './components/MessageInput';
import { Spinner } from 'react-bootstrap';

const SpinnerDivStyle = {
    display: 'flex',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
}

function removeFromArr(array, id) {
    let tempArr = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i].id !== id) {
            tempArr.push(array[i]);
        }
    }
    return tempArr;
}

class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            url: props.url,
            messages: [],
            messagesLength: 0,
            messagesLoaded: false,
            currentUser: {
                user: "Kirill",
                userId: "121314",
                avatar: null
            },
            messageToEditIndex: {},
            isEditMode: false,
        };
        this.receiveMessage = this.receiveMessage.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.editModeHander = this.editModeHander.bind(this);
        this.editMessage = this.editMessage.bind(this);
    }

    componentDidMount() {
        fetch(this.state.url)
            .then(resp => resp.json())
            .then((messages) => {
                this.setState({
                    messages: messages.map(message => ({
                        id: message.id,
                        userId: message.userId,
                        avatar: message.avatar,
                        user: message.user,
                        text: message.text,
                        createdAt: message.createdAt,
                        editedAt: message.editedAt
                    })),
                    messagesLength: messages.length,
                    messagesLoaded: true
                })
            })
    }

    receiveMessage(message) {
        this.state.messages.push(message);
        this.setState({ messagesLength: this.state.messages.length })
    }

    likeMessage(id) {
        const newArr = [...this.state.messages];
        newArr[id].isLiked = !this.state.messages[id].isLiked;

        this.setState({ messages: newArr });
    }

    deleteMessage(id) {
        let tempArr = removeFromArr([...this.state.messages], id);
        this.setState({ messages: tempArr });
    }

    editMessage(newMessage) {

        const { messages, messageToEditIndex } = this.state;
        const newArr = [...messages];

        newArr[messageToEditIndex] = newMessage;

        this.setState({
            messages: newArr,
            messageToEditIndex: null,
            isEditMode: false
        });
    }

    editModeHander(id) {
        this.setState({
            messageToEditIndex: id,
            isEditMode: !this.state.isEditMode
        });
    }

    render() {

        const {
            messages,
            messagesLength,
            currentUser,
            messageToEditIndex,
            isEditMode
        } = this.state;

        return (
            !this.state.messagesLoaded ?
                <div style={SpinnerDivStyle}><Spinner animation="border" variant="dark" /></div> :
                <div className='chat'>
                    <Header messages={messages} />
                    <MessageList
                        numberOfMessages={messagesLength}
                        messages={messages}
                        currentUser={currentUser}
                        likeMessageHandler={this.likeMessage}
                        deleteMessageHandler={this.deleteMessage}
                        editModeHandler={this.editModeHander}
                    />
                    <MessageInput
                        addMessageHandler={this.receiveMessage}
                        currentUser={currentUser}
                        editMessageHandler={this.editMessage}
                        isEditMode={isEditMode}
                        messageToEdit={messages[messageToEditIndex]}
                    />
                </div>
        )
    }
}

export default Chat;